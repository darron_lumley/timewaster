var User = require('../datasets/users');
var fs = require('fs-extra');
var path = require('path');

module.exports.updatePhoto = function(req, res) {
  var file = req.files.file;
  var userId = req.body.userId;

  var uploadDate = new Date().toISOString();

  var uploadDateClean = uploadDate.replace(/[|&;$%@"<>()+:/,\\]/g, "-");

  var tempPath = file.path;

  var targetPath = path.join(__dirname, "../uploads/" + userId + uploadDateClean + file.name);
  console.log(targetPath);
  var savePath = "/uploads/" + userId + uploadDate + file.name;

  fs.rename(tempPath, targetPath, function(err){
    if(err){
      console.log(err);
    } else {
      User.findById(userId, function(err, userData){
        var user = userData;
        user.image = savePath;
        user.save(function(err){
          if(err){
            console.log("Failed to save");
          } else {
            console.log("Save Successful");
          }
        })
      });
    }
  });
} ;