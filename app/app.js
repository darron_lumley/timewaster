(function () {
  angular.module('TimeWaste', ['ui.router', 'ngFileUpload'])
      .config(function ($stateProvider) {
        $stateProvider
            .state('signUp', {
              url: "/signup",
              templateUrl: "app/signup/signup.html",
              controller: "SignUpController",
              controllerAs: "signUp"
            })
            .state('editProfile', {
              url: "/editProfile",
              templateUrl: "app/profile/edit-profile-view.html",
              controller: "EditProfileController",
              controllerAs: "editProfileCtrl"
            });
      });
})();