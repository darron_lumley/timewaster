(function(){
  angular.module('TimeWaste').controller('NavigationController', ['$http', '$state', NavigationController]);

  function NavigationController ($http, $state){
    var me = this;

    me.loggedIn = !!localStorage['User-Data'];

    me.logUserIn = LogUserIn;

    function LogUserIn(){
      $http.post('/api/user/login', me.login)
          .success(function(response){
            localStorage.setItem('User-Data', JSON.stringify(response));
            me.loggedIn = true;
          })
          .error(function (error) {
            console.log(error);
          })
    }
  }
})();