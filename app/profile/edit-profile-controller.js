(function(){
  angular.module('TimeWaste').controller('EditProfileController', ['Upload', '$scope', '$state', '$http', EditProfile]);

  function EditProfile(Upload, $scope, $state, $http) {
    var me = this;
    me.user = JSON.parse(localStorage['User-Data']) || undefined;

    $scope.$watch(function() {
      return $scope.file;
    }, function() {
      UploadFile($scope.file);
    });

    function UploadFile(file){
      if(file){
        Upload.upload({
          url: 'api/profile/editPhoto',
          method: 'POST',
          data: {
            userId: me.user._id
          },
          file: file
        }).progress(function(evt){
          console.log('firing');
        }).success(function(data){

        }).error(function(error){
          console.log(error);
        });
      }
    }

  }
})();