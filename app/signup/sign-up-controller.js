(function () {
  angular.module('TimeWaste').controller('SignUpController', ['$state', '$http', SignUpController]);

  function SignUpController($state, $http) {
    var me = this;
    me.createUser = function () {
      $http.post('api/user/signup', me.newUser)
          .success(function (response) {
            console.log(response);
          })
          .error(function (error) {
            console.log(error)
          });
    }
  }
})();